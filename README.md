# ![Nimbug M0](Images/nimbug-icon.png){width=50px} Nimbug M0
### The World's Smallest SAMD21 Development Board

![PCB Front](Images/PCB_Front.png){width=32%} ![PCB Back](Images/PCB_Back.png){width=32%} ![PCB Dimensions](Images/PCB_Dimensions.png){width=33%}

![Schematic Diagram](Images/Schematic.png){width=100%}

## Pinouts

*In counter-clockwise order from the first pin, as denoted by the white circle near the bottom left corner of the board.*

|   Pad Label   |             Name             |  Function   |         MCU Pin         |  SwitchME Pad   | Trinket M0 Pin  |
| :-----------: | :--------------------------: | :---------: | :---------------------: | :-------------: | :-------------: |
|       G       |            (GND)             |   Ground    |      GND (10 + 28)      |       GND       |       Gnd       |
|       R       |           (Reset)            |    Reset    |       RESET (26)        |       RST       |       Rst       |
|       D       |           (SWDIO)            | Programming |        PA31 (32)        |       SD        | {Not Connected} |
|       C       |           (SWCLK)            | Programming |        PA30 (31)        |       SC        | {Not Connected} |
|       3       |           (3.3V)             |    Power    | VDDIN (30) + VDDANA (9) |       3V3       |        3V       |
|       M       | 8 / PIN_M / PIN_DOTSTAR_DATA |    GPIO     |        PA01 (2)         | {Not Connected} |  (DotStar MOSI) |
|       S       | 7 / PIN_S / PIN_DOTSTAR_CLK  |    GPIO     |        PA00 (1)         | {Not Connected} |  (DotStar SCK)  |
|       X       |        1 / A0 / PIN_X        |    GPIO     |        PA02 (3)         |       VO        |        1        |
|       Y       |        4 / A4 / PIN_Y        |    GPIO     |        PA06 (7)         |       TX        |        4        |
|       V       |        0 / A2 / PIN_V        |    GPIO     |        PA08 (11)        |       SDA       |        0        |
|       J       |        3 / A3 / PIN_J        |    GPIO     |        PA07 (8)         |       RX        |        3        |
|       U       |        2 / A1 / PIN_U        |    GPIO     |        PA09 (12)        |       SCL       |        2        |
|       -       |           (USB D-)           |     USB     |        PA24 (23)        |       D-        |  (MicroUSB D-)  |
|       +       |           (USB D+)           |     USB     |        PA25 (24)        |       D+        |  (MicroUSB D+)  |
|       5       |           (5V In)            |    Power    |     {NC} (U2 1 + 3)     |       5V        |       USB       |
| (Onboard LED) |       13 / LED_BUILTIN       |     LED     |        PA10 (13)        |  (Onboard LED)  |  (Onboard LED)  |

## Arduino IDE Board Manager URL

```
https://gitlab.com/nimrod-devices/Nimbug-M0/-/raw/main/BoardFile/package_nimrod_nimbug_index.json
```

## Background

This is essentially an Adafruit Trinket M0 that has been stripped down to it's absolute smallest size. It is designed to be used in tight spaces, or in USB related projects that require an off-board USB connector.

It has no mounting, electrical or mechanical. It simply has SMD pads exposing all of the important pins, just like the Trinket M0 but smaller and non-conductive on the bottom side. It is designed to be mounted with double-sided tape and to be soldered to with loose wires, in order to provide the maximum versatility.

The name of the board is inspired by "dead bug" soldering, where one glues an IC upside-down and solders components and wires directly to the legs.

## Differences from the Adafruit Trinket M0

Some hardware was removed from the board, in order to make it even smaller. All features can be re-created with external components using the exposed pads.
- APA102 RGB LED
  - Exposed pads S (7) and M (8) are the SCK and MOSI pins that were connected to the APA102.
  - Exposed pads 3 (3.3V) and G (GND) are the power rails that were connected to the APA102.
- USB Connector
  - Exposed pads + (USB D+) and - (USB D-) are the data lines that were connected to the microUSB port.
  - Exposed pads 5 (5V In) and G (GND) are the power lines that were connected to the microUSB port.
- Reset Button
  - Exposed pads R (Reset) and G (GND) are the pins that were connected to the reset button.
  - These 2 pads are intentionally placed next to each other, so that it is easy to short them out with a metal object like tweezers.
  - These are the 2 pads closest to the white circle near the bottom left corner of the board.
- Power LED
  - Exposed pads 3 (3.3V) and G (GND) are the voltage rails that were connected across the LED-resistor circuit.

## Inspiration

The idea for this board came from the numerous Nintendo Switch V1 modchips (payload injectors). Those devices started being made when people realized that a Trinket M0 could be used to run custom software on some consoles if you installed them onto the motherboard and loaded some special firmware onto them. Because the Switch has very little open space inside it, people started making and selling stripped-down versions of the Trinket M0 that were designed to be as small and easy to fit into the Switch as possible.

While Nintendo doesn't really like that particular use case, I thought the idea of an absolutely minimized Trinket M0 was really solid! Specifically, I immediately imagined using it for advanced [circuit-bent musical instruments](https://en.wikipedia.org/wiki/Circuit_bending), possibly even being able to add MIDI over USB! Because these devices I mod are so varied, it's basically impossible to design a USB-based board that mounts universally. With this board though, I can separate the problems of fitting a USB port and fitting the MCU board. Plus I don't have to worry about desoldering the microUSB and then tacking on to its minuscule pads, I just have 4 big USB pads broken out and ready to solder to.

In terms of hardware compatibility, this chip is closest to the "Rebug SwitchME M0" modchip. Indeed, it should be directly compatible with any firmware written for that board. Of course, the pads are laid out differently, so make sure you check the pinout diagram before you start soldering.

No `.uf2` firmware files are provided here, just the UF2 bootloader. An Arduino board definition file is provided to enable you to write your own code.

## Disclaimer

**THIS IS NOT A MODCHIP!!! THIS IS NOT A PAYLOAD INJECTOR!!! THIS IS NOT AN ANTI-PIRACY CIRCUMVENTION DEVICE!!!**

While the form factor of this device is based on various Nintendo Switch "modchips", the intent of this device is to act as a general-purpose MCU development board specialized for use in new or existing custom electronics projects, and/or for new product research and development. This repository does **NOT** contain any hardware or software which enables the circumvention of any security or anti-piracy measures, on any device, by any means. The bootloader provided is a customized version of the [Adafruit UF2 Bootloader](https://github.com/adafruit/uf2-samdx1) which simply enables the MCU to be programmed directly over a USB connection (i.e. without an intermediate programming interface device).

It is the sole responsibility of the end-user to ensure that the hardware and software files provided in this repository are used lawfully. No further disclaimers or warnings will be provided.

## Flashing the UF2 Bootloader

So you just finished assembling your own board? Congratulations! But sadly, you can't program it over USB yet. To get to that point, you have to actually put a small piece of code onto the chip that manages loading more code over USB (called the *bootloader*). And in order to get the bootloader onto the chip, you have to use a special device called a *programmer*.

The gold standard for programming these so-called `SAM` chips is the [SAM-ICE](https://www.microchip.com/en-us/development-tool/at91sam-ice), as it provides tons of high-tech features that high-level development on these chips requires. But we don't need all of that, all we care about is that it allows you to get the `.bin` bootloader file from your computer onto the chip. But this option is very expensive, very overkill, and also not generally able to be ordered.

The next best option is to use a [J-Link EDU Mini](https://www.segger.com/products/debug-probes/j-link/models/j-link-edu-mini/). This can also flash the bootloader, but it costs about $100 USD.

I don't currently own an SAM programmer, and I'm too poor to buy one like those listed above. So, can I still flash the bootloader myself?

Absolutely! You actually just need another Arduino-compatible SAMD dev board! As I understand it, pretty much any SAMD board that works with the Arduino platform and that already has it's bootloader flashed can be used in various ways to flash `.bin` files onto unprogrammed "virgin" SAMD21 chips. This works thanks to the ever-amazing Adafruit and their [DAP fork for SAMD devices](https://github.com/adafruit/Adafruit_DAP)!

I happen to own a MKRZERO, which uses a SAMD21G18A, and has the following features:
- Built-in microUSB connector
- SD card slot *(can store the bootloader `.bin` file on the SD card)*
- Rechargeable lithium battery support

This makes it a really good "host" device to program many boards with the bootloader, as it doesn't even need a computer to run. If hooked up to USB, you can view the logs of the flashing process on the serial monitor.

If you have a MKRZERO, or another Arduino-compatible SAMD dev board that has an SD card slot, you can upload [the Arduino sketch found here](https://gitlab.com/nimrod-devices/Nimbug-M0/-/tree/main/Bootloader/Programmer) to your board and use the serial monitor to see the status of your new programmer.

To program the Nimbug M0, you need to connect 5 wires:

|  Name  |  Host Pin  |  Nimbug M0 Pad  |
| :----: | :--------: | :-------------: |
|  GND   |    GND     |        G        |
| RESET  |     9      |        R        |
| SWDIO  |     12     |        D        |
| SWCLK  |     11     |        C        |
|  3.3v  |    VCC     |        3        |
